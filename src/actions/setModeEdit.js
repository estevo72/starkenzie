export const setModeEdit = (mode) => ({ type: "MODE_EDIT", mode });
export const setModeEditData = (data) => ({ type: "MODE_EDIT_DATA", data });

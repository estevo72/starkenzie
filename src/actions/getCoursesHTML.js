import axios from "axios";

const courseHTML = (listHTML) => ({
  type: "GET_LIST_HTML",
  listHTML,
});

export const getCoursesHTML = (token) => (dispatch) => {
  axios
    .get(`https://estevao-api.herokuapp.com/contents`, {
      headers: {
        Authorization: token,
      },
    })
    .then((res) => {
      const HTML = res.data.filter((item) => item.html);
      dispatch(courseHTML(HTML));
    });
};

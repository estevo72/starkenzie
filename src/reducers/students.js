const defaultState = { students: [], answer: {} };

const students = (state = defaultState, action) => {
  switch (action.type) {
    case "NEW_STUDENTS":
      const { students } = action;
      return { ...state, students };

    case "ANSWERS":
      const { answer } = action;
      return {
        ...state,
        answer,
      };

    case "LOGOUT":
      return defaultState;

    default:
      return state;
  }
};

export default students;

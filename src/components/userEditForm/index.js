import React from "react";
import "antd/dist/antd.css";
import { withRouter } from "react-router-dom";
import axios from "axios";
import { useSelector } from "react-redux";
import { message } from "antd";
import UserForm from '../userform'


const UserEditForm = (props) => {

  const userId = useSelector((state) => state.session.user.id);
  const userUser = useSelector((state) => state.session.user.user);
  const userEmail = useSelector((state) => state.session.user.email);
  const userCPF = useSelector((state) => state.session.user.cpf);
  const userPhone = useSelector((state) => state.session.user.cellphone);
  const userAddress = useSelector((state) => state.session.user.address);
  const userGrade = useSelector((state) => state.session.user.grade);
  const userToken = useSelector((state) => state.session.currentToken);
  const userNames = useSelector((state) => state.session.user.name);
  const userName = userNames.split(" ").slice(0, 1)
  const userLastname = userNames.split(" ").slice(-1);


  const onFinish = ({ name, lastName, user, email, cpf, phone, address, birth, grade, password }) => {
    axios
      .put(`https://estevao-api.herokuapp.com/users/${userId}`, {
        user: {
          name: `${name}  ${lastName}`,
          user: user,
          email: email,
          cpf: cpf,
          cellphone: phone,
          address: address,
          birth: birth,
          grade: grade,
          password: password,
          password_confirmation: password,
        }
      },
        {
          headers: {
            Authorization: userToken
          }
        }

      )
      .then(
        (res) => {
          if (res.status === 200) {
            message.success("Cadastro atualizado.", 1.5);
          }
        },
        (error) =>
          message.error("Cadastro não atualizado, verifique os campos.", 3)
      );
  };

  return (

    <UserForm
      userName={userName}
      userLastname={userLastname}
      userUser={userUser}
      userEmail={userEmail}
      userCPF={userCPF}
      userPhone={userPhone}
      userAddress={userAddress}
      userGrade={userGrade}
      onFinish={onFinish}
      buttonTitle={"ATUALIZAR"}
    />
  );
};



export default withRouter(UserEditForm);


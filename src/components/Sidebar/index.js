import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { Layout, Menu } from "antd";
import {
  LogoutOutlined,
  PieChartOutlined,
  TeamOutlined,
  UserOutlined,
  DatabaseOutlined,
} from "@ant-design/icons";
import { logout } from "../../actions/actionLogin";

const { Sider } = Layout;
const { SubMenu } = Menu;

const SideBar = () => {
  const userAuth = useSelector((state) => state.session.user.auth);
  const [collapsed, setCollapse] = useState(false);
  const onCollapse = () => setCollapse(!collapsed);
  const dispatch = useDispatch();

  return (
    <div style={collapsed ? { width: "80px" } : { width: "200px" }}>
      <Sider
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
        }}
        collapsible
        collapsed={collapsed}
        onCollapse={onCollapse}
      >
        <div
          className="logo"
          style={{
            fontSize: "25px",
            color: "white",
            textAlign: "center",
            height: "32px",
            background: "rgba(255, 255, 255, 0.2)",
            margin: "16px",
          }}
        >
          SK
        </div>
        <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
          <Menu.Item key="1" icon={<PieChartOutlined />}>
            <Link to="/dashboard/home" />
            Curso
          </Menu.Item>
          <Menu.Item icon={<DatabaseOutlined />}>
            <Link to="/dashboard/quiz" />
            Quiz
          </Menu.Item>
          <SubMenu key="sub1" icon={<UserOutlined />} title="Conta">
            <Menu.Item key="2" >
              <Link to="/dashboard/settings" />
              Opções
            </Menu.Item>
          </SubMenu>
          <Menu.Item key="3" icon={<TeamOutlined />}>
            <Link to="/dashboard/teachers" />
            Professores
          </Menu.Item>
          {userAuth > 1 && (
            <SubMenu key="sub2" icon={<DatabaseOutlined />} title="Controle">
              <Menu.Item key="4" icon={<UserOutlined />}>
                <Link to="/dashboard/waiting" />
                Novos Alunos
              </Menu.Item>
              <Menu.Item key="5" icon={<UserOutlined />}>
                <Link to="/dashboard/aceppted" />
                Aceitos
              </Menu.Item>
              <Menu.Item key="6" icon={<UserOutlined />}>
                <Link to="/dashboard/rejected" />
                Recusados
              </Menu.Item>
              <Menu.Item key="7" icon={<DatabaseOutlined />}>
                <Link to="/dashboard/content" />
                Conteudo
              </Menu.Item>
            </SubMenu>
          )}

          <Menu.Item
            key="9"
            icon={<LogoutOutlined />}
            onClick={() => dispatch(logout())}
          >
            <Link to="/">Sair</Link>
          </Menu.Item>
        </Menu>
      </Sider>
    </div>
  );
};

export default withRouter(SideBar);

import { combineReducers } from "redux";
import login from "./login";
import coursesList from "./coursesList";
import students from "./students";

const rootReducer = combineReducers({ session: login });
export type RootState = ReturnType<typeof rootReducer>;

export default combineReducers({ session: login, students, coursesList });

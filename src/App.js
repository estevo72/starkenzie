import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import SignUp from "./components/Signup";
import Login from "./components/Login/index.tsx";
import SiderBar from "./components/Sidebar";
import { withRouter, Route, Switch, Redirect } from "react-router-dom";
import Routes from "./Routes";
import { login } from "./actions/actionLogin";

const token = localStorage.getItem("currentToken");
const user = JSON.parse(localStorage.getItem("currentUser") || "{}");

const PrivateRoute = ({ component: Component, ...rest }) => {
  const tokenStore = useSelector((state) => state.session.currentToken);
  return (
    <Route
      {...rest}
      render={(props) =>
        tokenStore ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )
      }
    />
  );
};

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    if (token) {
      dispatch(login(token, user));
    }
  }, [dispatch]);

  return (
    <>
      <Switch>
        <Route exact path="/">
          <Login />
        </Route>
        <Route exact path="/cadastro">
          <SignUp />
        </Route>
        <PrivateRoute
          path="/dashboard"
          component={() => (
            <>
              <div style={{ display: "flex" }}>
                <SiderBar />
                <Routes />
              </div>
            </>
          )}
        />
      </Switch>
    </>
  );
};

export default withRouter(App);

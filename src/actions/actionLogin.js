import axios from "axios";
import { message } from "antd";
import { getCoursesHTML } from "./getCoursesHTML";
import { getCoursesCSS } from "./getCoursesCSS";
import { getCoursesJavascript } from "./getCoursesJavascript";

export const login = (currentToken, user) => ({
  type: "LOGIN",
  currentToken,
  user,
});

export const logout = () => {
  localStorage.clear();
  return { type: "LOGOUT" };
};

export const requestLogin = (user, password) => (dispatch) => {
  axios
    .post(`https://estevao-api.herokuapp.com/authenticate`, {
      user: user,
      password: password,
    })
    .then(
      (res) => {
        if(res.data.user.aceppted === "waiting"){
          message.error("Aguarde seu cadastro ser confirmado por um professor.", 3)
        }
        if(res.data.user.aceppted === "false"){
          message.error("Seu cadastro foi rejeitado, tente novamente na próxima turma.", 3)
        }
        if (res.status === 200) {
          localStorage.setItem("currentToken", res.data.auth_token);
          localStorage.setItem("currentUser", JSON.stringify(res.data.user));
          dispatch(login(res.data.auth_token, res.data.user));
          dispatch(getCoursesHTML(res.data.auth_token));
          dispatch(getCoursesCSS(res.data.auth_token));
          dispatch(getCoursesJavascript(res.data.auth_token));
        }
      },
      (error) => message.error("Falha ao autenticar", 3)
    );
};

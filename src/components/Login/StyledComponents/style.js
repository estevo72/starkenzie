import styled from "styled-components";

import BckImg from "./assets/bck-image.jpeg";

export const Body = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
`;

export const FormContainer = styled.div`
  width: 25%;
  height: 100vh;
  background: #d4dbf5;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const ImageContainer = styled.div`
  width: 75%;
  height: 100%;
  background-image: url(${BckImg});
  background-position: center;
  background-size: cover;
`;

export const ItemInfos = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: 6px 0px;
`;

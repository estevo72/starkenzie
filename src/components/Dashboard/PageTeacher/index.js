import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Avatar, Layout } from "antd";
import { UserOutlined } from "@ant-design/icons";
import axios from "axios";

const { Footer } = Layout;

const PageTeacher = () => {
  const [teachers, setTeachers] = useState([]);

  useEffect(() => {
    axios
      .get(`https://estevao-api.herokuapp.com/users`, {
        headers: {
          Authorization: localStorage.getItem("currentToken"),
        },
      })
      .then((res) => setTeachers(res.data.filter((item) => item.auth === 2)));
  }, []);

  return (
    <Layout className="site-layout" style={{ minHeight: "100vh" }}>
      <Header
        className="site-layout-background"
        style={{ background: "rgba(255, 255, 255, 0.2)", padding: 0 }}
      />
      <Content style={{ margin: "0 16px" }}>
        <div
          className="site-layout-background"
          style={{
            padding: 24,
            minHeight: 360,
            display: "flex",
            flexDirection: "row",
          }}
        >
          {teachers.map((teacher, key) => {
            return (
              <Container key={key}>
                <Header>
                  <Avatar
                    size={64}
                    style={{ backgroundColor: "#87d068" }}
                    icon={<UserOutlined />}
                  />
                  <Title>Instrutor</Title>
                </Header>
                <Content>
                  <span>{teacher.name}</span>
                  <span>{teacher.email}</span>
                </Content>
              </Container>
            );
          })}
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>StarKenzie ©2020</Footer>
    </Layout>
  );
};

export default PageTeacher;

const Container = styled.div`
  width: 500px;
  height: auto;
  background: #efefef;
  margin: 20px;
  padding: 20px;
  display: flex;
  flex-direction: column;
  flex-wrap: nowrap;
  justify-content: flex-start;
  align-items: center;
  border-radius: 6px;
`;

const Header = styled.div`
  width: 100%;
  padding: 20px;
  background: #dedede;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  border-radius: 6px;
  margin-bottom: 10px;
`;

const Title = styled.h1`
  margin: 10px 0px 0px 0px;
  padding: 0px;
  color: #fff;
  font-size: 20px;
`;

const Content = styled.div`
  width: 98%;
  height: auto;
  padding: 20px;
  background: #dedede;
  border-radius: 6px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

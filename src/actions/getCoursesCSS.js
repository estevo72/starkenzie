import axios from "axios";

const courseCSS = (listCSS) => ({
  type: "GET_LIST_CSS",
  listCSS,
});

export const getCoursesCSS = (token) => (dispatch) => {
  axios
    .get(`https://estevao-api.herokuapp.com/contents`, {
      headers: {
        Authorization: token,
      },
    })
    .then((res) => {
      const CSS = res.data.filter((item) => item.css);
      dispatch(courseCSS(CSS));
    });
};

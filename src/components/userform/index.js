import React from "react";
import styled from "styled-components";
import "antd/dist/antd.css";
import { Link, withRouter } from "react-router-dom";
import errorRules from "../Signup/validator";
import locale from "antd/es/date-picker/locale/pt_BR";
import BckImg from "./assets/img.jpg";
import { Form, Input, Button, DatePicker } from "antd";
import {
  PhoneOutlined,
  UserOutlined,
  IdcardOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import { MaskedInput } from "antd-mask-input";
import GradeSelect from "./GradeSelect";
import SetName from "./SetName";
import SetUser from "./SetUser";
import SetLastname from "./SetLastname";

const { Item } = Form;
const { Password } = Input;

const style = {
  width: "280px",
  height: "50px",
  borderRadius: "4px",
  outline: "none",
  fontSize: "15px",
  boxSizing: "border-box",
};

const UserForm = (props) => {
  return (
    <Body>
      <FormContainer>
        <Form
          style={{
            width: "580px",
            height: "auto",
            display: "flex",
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "space-between",
            alignItems: "center",
          }}
          name="normal_login"
          onFinish={(fieldValues) => props.onFinish(fieldValues)}
        >
          <Item
            name="name"
            rules={errorRules.name}
            initialValue={props.userName}
          >
            {SetName()}
          </Item>
          <Item
            name="lastName"
            rules={errorRules.name}
            initialValue={props.userLastname}
          >
            {SetLastname()}
          </Item>
          <Item
            name="user"
            rules={errorRules.user}
            initialValue={props.userUser}
          >
            {SetUser()}
          </Item>
          <Item
            name="email"
            rules={errorRules.email}
            initialValue={props.userEmail}
          >
            <Input
              style={{ ...style }}
              placeholder="Email"
              suffix={<UserOutlined />}
            />
          </Item>
          <Item name="cpf" rules={errorRules.cpf} initialValue={props.userCPF}>
            <MaskedInput
              mask="111.111.111-11"
              style={{ ...style }}
              placeholder="CPF"
              suffix={<IdcardOutlined />}
            />
          </Item>
          <Item
            name="phone"
            rules={errorRules.phone}
            initialValue={props.userPhone}
          >
            <MaskedInput
              mask="(11) 1 1111-1111"
              style={{ ...style }}
              placeholder="Telefone"
              suffix={<PhoneOutlined />}
            />
          </Item>
          <Item
            name="address"
            rules={errorRules.address}
            initialValue={props.userAddress}
          >
            <Input
              style={{ ...style, width: "580px" }}
              placeholder="Endereço"
              suffix={<HomeOutlined />}
            />
          </Item>
          {props.signup ? (
            <Item
              name="birth"
              rules={errorRules.birth}
              initialValue={props.userBirth}
            >
              <DatePicker
                style={{ ...style, width: "580px" }}
                format={"DD/MM/YYYY"}
                locale={locale}
              />
            </Item>
          ) : (
            <div></div>
          )}

          <Item
            name="grade"
            rules={errorRules.grade}
            initialValue={props.userGrade}
          >
            {GradeSelect()}
          </Item>

          {props.signup ? (
            <Item name="password" rules={errorRules.password}>
              <Password
                style={{ ...style, width: "580px" }}
                type="password"
                placeholder="Senha"
              />
            </Item>
          ) : (
            <div></div>
          )}
          <Item>
            <Button
              style={{
                height: "58px",
                width: "580px",
                marginTop: "12px",
                marginBottom: "8px",
                borderRadius: "4px",
                backgroundColor: "#146DFA",
                fontSize: "16px",
              }}
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              {props.buttonTitle}
            </Button>
            {props.signup ? <Link to="/">Login</Link> : <div></div>}
          </Item>
        </Form>
      </FormContainer>
      <BackgroundContainer />
    </Body>
  );
};

const Body = styled.div`
  width: 100%;
  height: 100vh;
  background-color: blue;
  display: flex;
  flex-direction: row;
`;

const FormContainer = styled.div`
  width: 40%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: #d4dbf5;
`;

const BackgroundContainer = styled.div`
  width: 60%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-image: url(${BckImg});
  background-position: center;
  background-size: cover;
`;

export default withRouter(UserForm);

import React from "react";
import { Button } from "antd";
import { DeleteOutlined } from "@ant-design/icons";

const ButtonDelete = ({ onClick }) => (
  <Button
    style={{
      marginTop: "6px",
      borderRadius: "6px",
      backgroundColor: "red",
      width: "100px",
      border: "none",
    }}
    onClick={onClick}
    type="primary"
  >
    <DeleteOutlined /> Deletar
  </Button>
);

export default ButtonDelete;

import React from "react";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { Button, message, Collapse } from "antd";
import { answers } from "../../../actions/actionStudents";
const { Panel } = Collapse;

const Students = (props) => {
  const dispatch = useDispatch();
  const students = useSelector((state) => state.students.students);
  const userToken = useSelector((state) => state.session.currentToken);
  const answered = useSelector((state) => state.students.answer);

  const acepptStudent = (id, aceppted) => {
    axios
      .put(
        `https://estevao-api.herokuapp.com/users/${id}`,
        {
          user: {
            aceppted: aceppted,
          },
        },
        {
          headers: {
            Authorization: userToken,
          },
        }
      )
      .then(
        (res) => {
          if (res.status === 200 && aceppted === "true") {
            message.success("Usuario foi aceito.", 1.5);
            props.refresh();
          }
          if (res.status === 200 && aceppted === "false") {
            message.error("Usuario foi recusado.", 1.5);
            props.refresh();
          }
        },
        (error) =>
          message.error("Houve algum erro ao aceitar ou recusar o aluno.", 1.5)
      );
  };

  const questionsAnswer = (id) => {
    axios
      .get(`https://estevao-api.herokuapp.com/users/${id}/answer_stundents`, {
        headers: {
          Authorization: userToken,
        },
      })
      .then((res) => {
        dispatch(answers({ [id]: res.data }));
      });
  };

  const performance = (obj) => {
    const totalQuestions = obj.length;
    let totalAcerts = 0;
    obj.map((answer) => {
       if(answer.answer === answer.question.answer_correct){
        totalAcerts++;
      }
      return answer.answer;
    });
    let tax = (totalAcerts/totalQuestions) * 100;
    return tax.toFixed(2);
  };

  const studentsMap = students.map((student) => {
    if (student.aceppted === props.status && props.status !== "true")
      return (
        <div
          className="site-layout-background"
          style={{
            padding: 24,
            width: "580px",
            height: "auto",
            display: "flex",
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "space-around",
            alignItems: "center",
            border: "1px solid #d9d9d9",
          }}
          key={student.id}
        >
          <div style={{ width: "300px" }}>{student.name}</div>
          <Button
            icon={<CheckOutlined />}
            type="primary"
            onClick={() => acepptStudent(student.id, "true")}
          >
            Aceitar
          </Button>
          <Button
            icon={<CloseOutlined />}
            type="primary"
            danger
            onClick={() => acepptStudent(student.id, "false")}
          >
            Recusar
          </Button>
        </div>
      );
    if (student.aceppted === props.status && props.status === "true")
      return (
        <div
          className="site-layout-background"
          style={{
            padding: 24,
            width: "580px",
            height: "auto",
            display: "flex",
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "space-around",
            alignItems: "center",
            border: "1px solid #d9d9d9",
          }}
          key={student.id}
        >
          <Collapse
            defaultActiveKey={[]}
            style={{ width: "300px" }}
            onChange={() => questionsAnswer(student.id)}
          >
            <Panel header={student.name} key={student.id}>
              {answered[student.id] ? (
                <>
                  <p>Perguntas respondias: {answered[student.id].length}</p>
                  <p>Taxa de acerto: {performance(answered[student.id])} %</p>
                </>
              ) : (
                <p></p>
              )}
            </Panel>
          </Collapse>
          <Button
            icon={<CloseOutlined />}
            type="primary"
            danger
            onClick={() => acepptStudent(student.id, "false")}
          >
            Recusar
          </Button>
        </div>
      );
  });

  return studentsMap;
};

export default Students;

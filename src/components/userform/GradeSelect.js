import React from "react";
import { Select } from "antd";
const { Option } = Select;

function GradeSelect() {
  return (
    <Select
      style={{
        border: "0px solid #D6D7DB",
        width: "580px",
        borderRadius: "4px",
      }}
      size="large"
      placeholder="Escolaridade"
    >
      <Option value="Fundamental incompleto">Fundamental incompleto</Option>
      <Option value="Fundametal completo">Fundamental Completo</Option>
      <Option value="Médio incompleto">Médio incompleto</Option>
      <Option value="Médio Cursando">Médio Cursando</Option>
      <Option value="Médio completo">Médio Completo</Option>
      <Option value="Superior Incompleto">Superior incompleto</Option>
      <Option value="Superior Cursando">Superior Cursando</Option>
      <Option value="Superior Completo">Superior completo</Option>
    </Select>
  );
}

export default GradeSelect;

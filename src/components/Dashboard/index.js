import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import { Layout } from "antd";
import "./style.css";
import { Modal } from "antd";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { logout } from "../../actions/actionLogin";
import PageCourse from "./PageCourse";
import axios from "axios";

const { confirm } = Modal;
const { Header, Content, Footer } = Layout;

const DashBoard = (props) => {
  const dispatch = useDispatch();
  const userId = useSelector((state) => state.session.user.id);
  const userName = useSelector((state) => state.session.user.name);
  const userToken = useSelector((state) => state.session.currentToken);
  const terms = useSelector((state) => state.session.user.terms);

  const showPromiseConfirm = () => {
    confirm({
      width: 850,
      title: `Bem vindo, ${userName}`,
      icon: <ExclamationCircleOutlined />,
      content:
        "Aqui StarKenzie você irá aprender as tecnologias mais usadas no mercado de trabalho, então se esforce e lembre-se: Organize seus horários e estude no minimo um hora por dia. Faça o melhor que puder, todos os seus dados nesse teste serão levados em consideração para que seja aceito no curso.",
      onOk() {
        axios
          .put(
            `https://estevao-api.herokuapp.com/users/${userId}`,
            {
              user: {
                terms: true,
              },
            },
            {
              headers: {
                Authorization: userToken,
              },
            }
          )
          .then((res) => {
            console.log(res);
          });
      },
      onCancel() {
        dispatch(logout());
      },
    });
  };

  if (terms !== true) {
    showPromiseConfirm();
  }

  return (
    <Layout className="site-layout" style={{ minHeight: "100vh" }}>
      <Header
        className="site-layout-background"
        style={{ background: "rgba(255, 255, 255, 0.2)", padding: 0 }}
      />
      <Content style={{ margin: "0 16px" }}>
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 360 }}
        >
          <PageCourse />
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>StarKenzie ©2020</Footer>
    </Layout>
  );
};

export default withRouter(DashBoard);

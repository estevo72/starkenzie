export const delModuleHtml = (key) => ({ type: "DEL_MODULE_HTML", key });
export const delModuleCSS = (key) => ({ type: "DEL_MODULE_CSS", key });
export const delModuleJS = (key) => ({ type: "DEL_MODULE_JAVASCRIPT", key });

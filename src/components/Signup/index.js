import React from "react";
import "antd/dist/antd.css";
import { withRouter } from "react-router-dom";
import axios from "axios";
import { requestLogin } from "../../actions/actionLogin";
import { useDispatch } from "react-redux";
import { message } from "antd";
import UserForm from '../userform'


const SignUp = (props) => {
  const dispatch = useDispatch();


  const onFinish = ({ name, lastName, user, email, cpf, phone, address, birth, grade, password }) => {
    axios
      .post(`https://estevao-api.herokuapp.com/users`, {
        user: {
          name: `${name}  ${lastName}`,
          user: user,
          email: email,
          cpf: cpf,
          cellphone: phone,
          address: address,
          birth: birth,
          grade: grade,
          password: password,
          password_confirmation: password,
          auth: 1,
          aceppted: "waiting",
          terms: false
        },
      })
      .then(
        (res) => {
          if (res.status === 201) {
            message.success("Cadastro realizado.", 1.5);
            setTimeout(() => {
              dispatch(requestLogin(user, password));
            }, 1500);
          }
        },
        (error) =>
          message.error("Cadastro não realizado, verifique os campos.", 3)
      );
  };

  return (

    <UserForm
      onFinish={onFinish}
      buttonTitle={"CADASTRAR"}
      signup={true}
    />
  );
};



export default withRouter(SignUp);

